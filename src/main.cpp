#include <iostream>
#include <map>
#include <thread>
#include <utility>

#include "tutils/Date.hpp"
#include "tutils/Log.hpp"
#include "tutils/Byte.hpp"
#include "tutils/Clock.hpp"
#include "tutils/RealTimeClock.hpp"
#include "tutils/TickClock.hpp"
#include "tutils/json/Object.hpp"
#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class Sprite : public tu::json::Serializable, tu::json::Deserializable {

public:
    enum Type {
        NONE,
        MAGIC,
        FOREST,
        WATER,
        __LENGTH
    };

    static std::string getTypeName(Type type) {
        switch (type) {
            case NONE:   return "NONE";
            case MAGIC:  return "MAGIC";
            case FOREST: return "FOREST";
            case WATER:  return "WATER";
            default:     return "(unknown)";
        }
    }



    std::pair<int, int> position;
    std::map<std::string, float> stats;
    std::string name;
    std::string description;
    Type type;

    Sprite(std::string name, Type type) {
        position = std::make_pair(3, 5);
        stats = std::map<std::string, float>({
            {"strength", rand() % 100 / 10.f},
            {"speed", rand() % 100 / 10.f},
            {"intelligence", rand() % 100 / 10.f}
        });
        description = "";
        if (type == Type::NONE) {
            this->type = Type(rand() % Type::__LENGTH);
        } else {
            this->type = type;
        }

        switch (rand() % 5) {
            case 0:
                this->name = std::string("Tiny ");
                break;

            case 1:
                this->name = std::string("Huge ");
                break;
        }
        this->name.append(name);
    }

    Sprite(const Sprite& spr) {
        position    = spr.position;
        stats       = spr.stats;
        name        = spr.name;
        description = spr.description;
        type        = spr.type;
    }

    std::string toString() const {
        return tu::String::str("<Sprite",
            ": position=", position,
            ", stats=", stats,
            ", name=", name,
            ", description=", description,
            ", type=", getTypeName(type),
            ">");
    }

    void loadJSONEx(std::shared_ptr<tu::json::Node> json) {
        
    }

    std::shared_ptr<tu::json::Node> toJSON() const {
        using tu::json::Node;
        auto root = std::make_shared<tu::json::Object>();
        root->setProperty("position",    Node::from(position)   )
            ->setProperty("stats",       stats      )
            ->setProperty("name",        name       )
            ->setProperty("description", description)
            ->setProperty("type",        long(type) );
        return root;
    }

};


void start() {
    srand(time(NULL));

    using namespace tu;

    Date date = Date::now();
    Log::log("Today's date: ", date);

    Timestamp start = Timestamp::now();

    std::map<std::string, int> map = std::map<std::string, int>();
    map["abc"] = 123;
    map["def"] = 234;
    map["abc"] = 345;
    Log::log("Map: ", map);

    std::vector<Sprite> sprites = std::vector<Sprite>();
    for (int i = 0; i < 1000; i++) {
        sprites.clear();
        sprites.push_back(Sprite("Wolf", Sprite::FOREST));
        sprites.push_back(Sprite("Shark", Sprite::WATER));
        sprites.push_back(Sprite("Ghost", Sprite::MAGIC));
    }

    Sprite player("Human", Sprite::NONE);
    player.description = "It's you!";
    sprites.push_back(player);

    Log::log("Sprites:\n", sprites);

    Timestamp end = Timestamp::now();
    int diff = start.diff(end).count();
    Log::log("Timestamps: ", start, " ", end, " (diff: ", diff, ")");

    Log::info(diff, " = ", Clock<>::formatTime(diff));

    diff = 2147483647;
    Log::info(diff, " = ", Clock<>::formatTime(diff));


    /*
    RealTimeClock<> rtclock = RealTimeClock<>();
    rtclock.reset();
    for (int i = 0; i < 5; i++) {
        Log::info("rtclock: ", rtclock);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    rtclock.pause();
    rtclock.set(10000000000); // In microseconds, because of the default RealTimeClock template argument
    for (int i = 0; i < 5; i++) {
        Log::info("rtclock: ", rtclock);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    rtclock.unpause();
    for (int i = 0; i < 5; i++) {
        Log::info("rtclock: ", rtclock);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    // Define unit as milliseconds (instead of default microseconds)
    // Also, hide the (P) in the string form when paused
    RealTimeClock<std::milli, int64_t, false> milliClock = RealTimeClock<std::milli, int64_t, false>();
    milliClock.reset();
    milliClock.pause();
    milliClock.set(1337); // In milliseconds, because of the std::milli RealTimeClock template argument
    for (int i = 0; i < 5; i++) {
        Log::info("milliClock (paused): ", milliClock, " raw: ", milliClock.get());
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    milliClock.unpause();
    for (int i = 0; i < 5; i++) {
        Log::info("milliClock: ", milliClock, " raw: ", milliClock.get());
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }a
    */


    // TickClock<> tickClock = TickClock<>(60);


    std::string json = R"({"text": "str\"ing", "abc": 123, "obj": {"a": 1, "b": 2}})";
    auto node = json::Node::fromJSON(json);
    Log::log("Type: ", node->getType());

    std::string invalidJson = R"({"text": this is invalid json, "abc": 123})";
    auto node2 = json::Node::fromJSON(invalidJson);
    Log::log("Invalid JSON type: ", node2->getType());


    std::string str1, str2;
    int diff1, diff2;

    {
        const int iterations = 100000;
        Timestamp start, end;

        start = Timestamp::now();
        for (int i = 0; i < iterations; i++) {
            str1 = node->toString();
        }
        end = Timestamp::now();
        diff1 = start.diff(end).count();

        start = Timestamp::now();
        for (int i = 0; i < iterations; i++) {
            str2 = node->toJSONString();
        }
        end = Timestamp::now();
        diff2 = start.diff(end).count();
    }

    Log::log("toString: ", str1, " toJSON: ", str2);
    Log::log("toString: ", Clock<>::formatTime(diff1), " toJSON: ", Clock<>::formatTime(diff2));



    {
        json::Object* obj = node->cast<json::Object>();

        Log::log(obj->getProperty("abc")->getOr(-1));
    }
}


int main() {
    try {
        start();
    } catch (tu::Exception e) {
        e.print();
    }
}
