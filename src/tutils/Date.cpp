#include "Date.hpp"


#include <sstream>
#include <iomanip>



namespace tu {


Date Date::now() {
    return Date(std::chrono::system_clock::now());
}


std::time_t Date::getUNIXTimestamp() const {
    return timestamp;
}

std::chrono::system_clock::time_point Date::getTimePoint() const {
    auto epoch = std::chrono::time_point<std::chrono::system_clock>();
    auto sinceEpoch = std::chrono::milliseconds(timestamp);
    return epoch + sinceEpoch;
}

Timestamp Date::getTimestamp() const {
    return Timestamp(*this);
}


Date& Date::setUNIXTimestamp(const std::time_t& timestamp) {
    this->timestamp = timestamp;
    return *this;
}

Date& Date::setTimePoint(const std::chrono::system_clock::time_point& timePoint) {
    this->timestamp = std::chrono::system_clock::to_time_t(timePoint);
    return *this;
}

Date& Date::setTimestamp(const Timestamp& timestamp) {
    return this->setTimePoint(timestamp.getTimePoint());
}


Date::operator std::time_t() const {
    return this->getUNIXTimestamp();
}

Date::operator std::chrono::system_clock::time_point() const {
    return this->getTimePoint();
}

Date::operator Timestamp() const {
    return this->getTimestamp();
}


Date::Date() {
    setTimestamp(0);
}

Date::Date(const std::time_t& timestamp) {
    setTimestamp(timestamp);
}

Date::Date(const std::chrono::system_clock::time_point& timePoint) {
    setTimePoint(timePoint);
}

Date::Date(const Timestamp& timestamp) {
    setTimestamp(timestamp);
}



std::string Date::toString() const {
    auto tm = std::localtime(&timestamp);
    std::stringstream ss;
    ss << std::put_time(tm, "%Y-%m-%d %H:%M:%S");
    return ss.str();
}

std::string Date::toSnakeCaseString() const {
    auto tm = std::localtime(&timestamp);
    std::stringstream ss;
    ss << std::put_time(tm, "%Y_%m_%d_%H_%M_%S");
    return ss.str();
}


};
