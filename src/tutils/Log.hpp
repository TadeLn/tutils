#pragma once


#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <chrono>
#include <mutex>

#include "Date.hpp"
#include "String.hpp"



#define ANSI_COLOR_BLACK   "\u001b[30m"
#define ANSI_COLOR_RED     "\u001b[31m"
#define ANSI_COLOR_GREEN   "\u001b[32m"
#define ANSI_COLOR_YELLOW  "\u001b[33m"
#define ANSI_COLOR_BLUE    "\u001b[34m"
#define ANSI_COLOR_MAGENTA "\u001b[35m"
#define ANSI_COLOR_CYAN    "\u001b[36m"
#define ANSI_COLOR_WHITE   "\u001b[37m"

#define ANSI_COLOR_BLACK_BRIGHT   "\u001b[30;1m"
#define ANSI_COLOR_RED_BRIGHT     "\u001b[31;1m"
#define ANSI_COLOR_GREEN_BRIGHT   "\u001b[32;1m"
#define ANSI_COLOR_YELLOW_BRIGHT  "\u001b[33;1m"
#define ANSI_COLOR_BLUE_BRIGHT    "\u001b[34;1m"
#define ANSI_COLOR_MAGENTA_BRIGHT "\u001b[35;1m"
#define ANSI_COLOR_CYAN_BRIGHT    "\u001b[36;1m"
#define ANSI_COLOR_WHITE_BRIGHT   "\u001b[37;1m"

#define ANSI_COLOR_CUSTOM(number) std::string("\u001b[38;5;") + std::to_string(number) + "m"

#define ANSI_COLOR_RESET   "\u001b[0m"



namespace tu {


class Log {


public:

    // Log a message to std::cout + file
    template <typename ...Args>
    static void log(Args ...args) {
        print("", std::cout, args...);
    }

    // Log an info message (blue) to std::cout + file
    template <typename ...Args>
    static void info(Args ...args) {
        print(ANSI_COLOR_BLUE, std::cout, "[INFO] ", args...);
    }

    // Log a warning message (yellow) to std::cerr + file
    template <typename ...Args>
    static void warn(Args ...args) {
        print(ANSI_COLOR_YELLOW, std::cerr, "[WARN] ", args...);
    }

    // Log an error message (red) to std::cerr + file
    template <typename ...Args>
    static void error(Args ...args) {
        print(ANSI_COLOR_RED, std::cerr, "[ERROR] ", args...);
    }


    // Open the log file
    static bool open(std::string file);

    // Close the log file
    static void close();


    static std::mutex printLock;

    // Raw print function
    template <typename ...Args>
    static void print(std::string colorString, std::ostream& stream, Args ...args) {
        Log::printLock.lock();
        
        std::string strNoColor;
        std::string str;
        strNoColor = String::str("[", Date::now(), "] ", args...);

        if (colorString.empty()) {
            str = strNoColor;
        } else {
            str = String::str(colorString, "[", Date::now(), "] ", args..., ANSI_COLOR_RESET);
        }

        stream << (std::string)str << std::endl;
        if (fileOpen) {
            getFile() << (std::string)strNoColor << std::endl;
        }

        Log::printLock.unlock();
    }

    static bool isFileOpen();
    static std::ofstream& getFile();


private:
    static bool lock;
    static bool fileOpen;
    static std::ofstream file;

};


};
