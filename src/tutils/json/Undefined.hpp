#pragma once


#include <memory>
#include <optional>

#include "lib/nlohmann/json.hpp"

#include "Value.hpp"



namespace tu {

namespace json {


// json::Undefined is returned when no value is found
class Undefined : public Value {


public:
    nlohmann::json toNlohmann();
    static std::shared_ptr<Undefined> fromNlohmann(const nlohmann::json& j);

    Undefined();

    std::string toString() const;


};


}

}
