#include "Null.hpp"


#include "tutils/Exception.hpp"



namespace tu {

namespace json {



nlohmann::json Null::toNlohmann() {
    return nlohmann::json(nullptr);
}

std::shared_ptr<Null> Null::fromNlohmann(const nlohmann::json& j) {
    if (j.is_null()) {
        return std::make_shared<Null>();
    } else {
        throw Exception("The nlohmann::json value is the wrong type", EXCTX);
    }
}



Null::Null() : Value(Node::Type::_NULL) {}



std::string Null::toString() const {
    return "null";
}



}

}
