#include "Deserializable.hpp"



namespace tu {

namespace json {


bool Deserializable::loadJSON(std::shared_ptr<Node> json) {
    try {
        this->loadJSONEx(json);
        return true;
    } catch (tu::Exception& e) {
        return false;
    }
}


}

}
