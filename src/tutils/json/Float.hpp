#pragma once


#include <memory>
#include <optional>

#include "lib/nlohmann/json.hpp"

#include "Value.hpp"



namespace tu {

namespace json {


// json::Float is an unsigned whole number, like 0, 23, 453, 9000000000
class Float : public Value {


public:
    double value;
    std::optional<double> getDouble() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<Float> fromNlohmann(const nlohmann::json& j);

    Float(double value = 0);

    std::string toString() const;


};


}

}
