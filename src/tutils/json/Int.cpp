#include "Int.hpp"



namespace tu {

namespace json {



std::optional<std::int64_t> Int::getLong() const {
    return std::optional(value);
}



nlohmann::json Int::toNlohmann() {
    return nlohmann::json(value);
}

std::shared_ptr<Int> Int::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Int> result = std::make_shared<Int>();
    result->value = j.get<std::int64_t>();
    return result;
}



Int::Int(std::int64_t value) : Value(Node::Type::INT) {
    this->value = value;
}



std::string Int::toString() const {
    return std::to_string(value);
}



}

}
