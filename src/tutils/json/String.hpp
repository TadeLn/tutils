#pragma once


#include <memory>
#include <optional>
#include <string>

#include "Value.hpp"



namespace tu {

namespace json {


// json::String is a text string, like "abc", "Hello world!", "Lorem ipsum"
class String : public Value {


public:
    std::string value;
    std::optional<std::string> getString() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<String> fromNlohmann(const nlohmann::json& j);

    String(std::string value = "");

    std::string toString() const;


};


}

}
