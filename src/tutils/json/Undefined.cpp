#include "Undefined.hpp"


#include "tutils/Exception.hpp"



namespace tu {

namespace json {



nlohmann::json Undefined::toNlohmann() {
    throw Exception("You cannot save an undefined value", EXCTX);
}

std::shared_ptr<Undefined> Undefined::fromNlohmann(const nlohmann::json& j) {
    throw Exception("You cannot load an undefined value", EXCTX);
}



Undefined::Undefined() : Value(Node::Type::UNDEFINED) {}



std::string Undefined::toString() const {
    return "undefined";
}



}

}
