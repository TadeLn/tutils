#pragma once


#include <memory>
#include <optional>

#include "lib/nlohmann/json.hpp"

#include "Value.hpp"



namespace tu {

namespace json {


// json::Uint is an unsigned whole number, like 0, 23, 453, 9000000000
class Uint : public Value {


public:
    std::uint64_t value;
    std::optional<std::uint64_t> getUlong() const;
    std::optional<std::int64_t> getLong() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<Uint> fromNlohmann(const nlohmann::json& j);

    Uint(std::uint64_t value = 0);

    std::string toString() const;


};


}

}
