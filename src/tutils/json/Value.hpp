#pragma once


#include "Node.hpp"



namespace tu {


namespace json {


// json::Value is any primitive value like: 0, 123.456, false, "text"
class Value : public Node {


public:
    static std::shared_ptr<Node> fromNlohmann(const nlohmann::json& j);


protected:
    Value(Type type);


};


}


}