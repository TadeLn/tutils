#include "Bool.hpp"



namespace tu {

namespace json {



std::optional<bool> Bool::getBool() const {
    return std::optional(value);
}



nlohmann::json Bool::toNlohmann() {
    return nlohmann::json(value);
}

std::shared_ptr<Bool> Bool::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Bool> result = std::make_shared<Bool>();
    result->value = j.get<bool>();
    return result;
}



Bool::Bool(bool value) : Value(Node::Type::BOOL) {
    this->value = value;
}



std::string Bool::toString() const {
    return value ? "true" : "false";
}



}

}
