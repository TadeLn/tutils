#pragma once


#include <memory>
#include <optional>

#include "lib/nlohmann/json.hpp"

#include "Value.hpp"



namespace tu {

namespace json {


// json::Null is a null value
class Null : public Value {


public:
    nlohmann::json toNlohmann();
    static std::shared_ptr<Null> fromNlohmann(const nlohmann::json& j);

    Null();

    std::string toString() const;


};


}

}
