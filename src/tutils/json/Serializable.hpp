#pragma once


#include "Node.hpp"



namespace tu {

namespace json {


class Serializable : public Stringable {


public:
    virtual std::shared_ptr<Node> toJSON() const = 0;

    virtual std::string toString() const;


};


}

}
