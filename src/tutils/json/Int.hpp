#pragma once


#include <memory>
#include <optional>
#include <string>

#include "Value.hpp"



namespace tu {

namespace json {


// json::Int is a number without a floating point, like 0, 3, -6, 9000000000
class Int : public Value {


public:
    std::int64_t value;
    std::optional<std::int64_t> getLong() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<Int> fromNlohmann(const nlohmann::json& j);

    Int(std::int64_t value = 0);

    std::string toString() const;


};


}

}
