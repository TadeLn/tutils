#include "Value.hpp"


#include <sstream>
#include <memory>
#include <fstream>

#include "tutils/Exception.hpp"
#include "Value.hpp"
#include "Uint.hpp"
#include "Int.hpp"
#include "Float.hpp"
#include "String.hpp"
#include "Bool.hpp"
#include "Null.hpp"



namespace tu {

namespace json {


std::shared_ptr<Node> Value::fromNlohmann(const nlohmann::json& j) {
    if (!j.is_primitive()) {
        throw Exception("This json element is not primitive", EXCTX);
    }

    if (j.is_number()) {
        if (j.is_number_integer()) {
            if (j.is_number_unsigned()) {
                return Uint::fromNlohmann(j);
            } else {
                return Int::fromNlohmann(j);
            }
        } else if (j.is_number_float()) {
            return Float::fromNlohmann(j);
        } else {
            throw Exception("Unknown json type", EXCTX);
        }
    } else if (j.is_string()) {
        return String::fromNlohmann(j);
    } else if (j.is_boolean()) {
        return Bool::fromNlohmann(j);
    } else if (j.is_null()) {
        return Null::fromNlohmann(j);
    } else {
        throw Exception("Unknown json type", EXCTX);
    }
}

Value::Value(Type type) : Node(type) {}



}

}
