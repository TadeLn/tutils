#include "Object.hpp"


#include "Undefined.hpp"
#include "tutils/String.hpp"



namespace tu {

namespace json {



std::optional<Node::dictionary> Object::getDictionary() const {
    return std::optional(nodes);
}



std::optional<std::shared_ptr<Node>> Object::getPropertyO(const std::string& key) const {
    auto it = nodes.find(key);
    if (it != nodes.end()) {
        return std::optional<std::shared_ptr<Node>>(it->second);
    } else {
        return std::optional<std::shared_ptr<Node>>();
    }
}

Node* Object::setProperty(const std::string& key, std::shared_ptr<Node> node) {
    nodes[key] = node;
    return this;
}



nlohmann::json Object::toNlohmann() {
    nlohmann::json j = nlohmann::json::object();
    for (auto it : nodes) {
        j[it.first] = it.second->toNlohmann();
    }
    return j;
}

std::shared_ptr<Object> Object::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Object> result = std::make_shared<Object>();
    for (auto it = j.begin(); it != j.end(); ++it) {
        result->nodes[it.key()] = Node::fromNlohmann(it.value());
    }
    return result;
}



Object::Object(dictionary values) : Node(Node::Type::OBJECT) {
    nodes = values;
}



// WARNING: this function does not return proper JSON
std::string Object::toString() const {
    return String::fromMap(nodes, [](std::string key, std::shared_ptr<Node> value){
        return "\"" + key + "\": " + value->toString();
    });
}



}

}
