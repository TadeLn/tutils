#include "Serializable.hpp"


namespace tu {

namespace json {


std::string Serializable::toString() const {
    return this->toJSON()->toString();
}


}

}
