#include "Node.hpp"


#include <sstream>
#include <memory>
#include <fstream>
#include <iostream>

#include "tutils/Exception.hpp"
#include "Undefined.hpp"
#include "Null.hpp"
#include "Int.hpp"
#include "Uint.hpp"
#include "Float.hpp"
#include "String.hpp"
#include "Bool.hpp"
#include "Value.hpp"
#include "Array.hpp"
#include "Object.hpp"



namespace tu {

namespace json {



Node::Type Node::getType() {
    return this->type;
}



std::shared_ptr<Node> Node::from() {
    return std::make_shared<Null>();
}

template<>
std::shared_ptr<Node> Node::from(bool value) {
    return std::make_shared<Bool>(value);
}

template<>
std::shared_ptr<Node> Node::from(std::int64_t value) {
    return std::make_shared<Int>(value);
}

template<>
std::shared_ptr<Node> Node::from(std::uint64_t value) {
    return std::make_shared<Uint>(value);
}

template<>
std::shared_ptr<Node> Node::from(double value) {
    return std::make_shared<Float>(value);
}

template<>
std::shared_ptr<Node> Node::from(std::string value) {
    return std::make_shared<String>(value);
}

template<>
std::shared_ptr<Node> Node::from(array value) {
    return std::make_shared<Array>(value);
}

template<>
std::shared_ptr<Node> Node::from(dictionary value) {
    return std::make_shared<Object>(value);
}

template<>
std::shared_ptr<Node> Node::from(std::initializer_list<Node::array::value_type> values) {
    return std::make_shared<Array>(Node::array(values));
}

template<>
std::shared_ptr<Node> Node::from(std::initializer_list<Node::dictionary::value_type> values) {
    return std::make_shared<Object>(Node::dictionary(values));
}



template<>
Undefined* Node::cast() {
    if (type == Type::UNDEFINED) return (Undefined*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Null* Node::cast() {
    if (type == Type::_NULL) return (Null*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Bool* Node::cast() {
    if (type == Type::BOOL) return (Bool*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Int* Node::cast() {
    if (type == Type::INT) return (Int*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Uint* Node::cast() {
    if (type == Type::UINT) return (Uint*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Float* Node::cast() {
    if (type == Type::FLOAT) return (Float*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
String* Node::cast() {
    if (type == Type::STRING) return (String*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Array* Node::cast() {
    if (type == Type::ARRAY) return (Array*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}

template<>
Object* Node::cast() {
    if (type == Type::OBJECT) return (Object*)this;
    throw tu::Exception("Cannot cast", EXCTX);
}



std::optional<std::int8_t> Node::getByte() const {
    auto v = this->getLong();
    if (v.has_value()) return std::optional<std::int8_t>(v.value());
    else               return std::optional<std::int8_t>();
}

std::optional<std::uint8_t> Node::getUByte() const {
    auto v = this->getUlong();
    if (v.has_value()) return std::optional<std::uint8_t>(v.value());
    else               return std::optional<std::uint8_t>();
}

std::optional<std::int16_t> Node::getShort() const{
    auto v = this->getLong();
    if (v.has_value()) return std::optional<std::int16_t>(v.value());
    else               return std::optional<std::int16_t>();
}

std::optional<std::uint16_t> Node::getUshort() const {
    auto v = this->getUlong();
    if (v.has_value()) return std::optional<std::uint16_t>(v.value());
    else               return std::optional<std::uint16_t>();
}

std::optional<std::int32_t> Node::getInt() const {
    auto v = this->getLong();
    if (v.has_value()) return std::optional<std::int32_t>(v.value());
    else               return std::optional<std::int32_t>();
}

std::optional<std::uint32_t> Node::getUint() const {
    auto v = this->getUlong();
    if (v.has_value()) return std::optional<std::uint32_t>(v.value());
    else               return std::optional<std::uint32_t>();
}

std::optional<float> Node::getFloat() const {
    auto v = this->getDouble();
    if (v.has_value()) return std::optional<float>(v.value());
    else               return std::optional<float>();
}

std::optional<bool> Node::getBool() const {
    return std::optional<bool>();
}

std::optional<std::int64_t> Node::getLong() const {
    auto v = this->getUlong();
    if (v.has_value()) return std::optional<std::int64_t>(v.value());
    else               return std::optional<std::int64_t>();
}

std::optional<std::uint64_t> Node::getUlong() const {
    return std::optional<std::uint64_t>();
}

std::optional<double> Node::getDouble() const {
    auto v = this->getLong();
    if (v.has_value()) return std::optional<double>(v.value());
    else               return std::optional<double>();
}

std::optional<std::string> Node::getString() const {
    return std::optional<std::string>();
}

std::optional<Node::array> Node::getArray() const {
    return std::optional<Node::array>();
}

std::optional<Node::dictionary> Node::getDictionary() const {
    return std::optional<Node::dictionary>();
}



template<>
std::optional<bool> Node::get() {
    return this->getBool();
}

template<>
std::optional<std::int8_t> Node::get() {
    return this->getByte();
}

template<>
std::optional<std::uint8_t> Node::get() {
    return this->getUByte();
}

template<>
std::optional<std::int16_t> Node::get() {
    return this->getShort();
}

template<>
std::optional<std::uint16_t> Node::get() {
    return this->getUshort();
}

template<>
std::optional<std::int32_t> Node::get() {
    return this->getInt();
}

template<>
std::optional<std::uint32_t> Node::get() {
    return this->getUint();
}

template<>
std::optional<float> Node::get() {
    return this->getFloat();
}

template<>
std::optional<std::int64_t> Node::get() {
    return this->getLong();
}

template<>
std::optional<std::uint64_t> Node::get() {
    return this->getUlong();
}

template<>
std::optional<double> Node::get() {
    return this->getDouble();
}

template<>
std::optional<std::string> Node::get() {
    return this->getString();
}

template<>
std::optional<Node::array> Node::get() {
    return this->getArray();
}

template<>
std::optional<Node::dictionary> Node::get() {
    return this->getDictionary();
}



std::shared_ptr<Node> Node::getProperty(const std::string& key) const {
    std::optional<std::shared_ptr<Node>> v = this->getPropertyO(key);
    if (v.has_value()) {
        return v.value();
    } else {
        return std::make_shared<Undefined>();
    }
}

std::shared_ptr<Node> Node::getPropertyEx(const std::string& key) const {
    std::optional<std::shared_ptr<Node>> v = this->getPropertyO(key);
    if (v.has_value()) {
        return v.value();
    } else {
        throw tu::Exception("Property \"" + key + "\" does not exist", EXCTX);
    }
}

std::optional<std::shared_ptr<Node>> Node::getPropertyO(const std::string& key) const {
    return std::optional<std::shared_ptr<Node>>();
}

bool Node::hasProperty(const std::string& key) const {
    return getPropertyO(key).has_value();
}

Node* Node::setProperty(const std::string& key, std::shared_ptr<Node> node) {
    return this;
}



std::shared_ptr<Node> Node::getElement(std::size_t index) const {
    std::optional<std::shared_ptr<Node>> v = this->getElementO(index);
    if (v.has_value()) {
        return v.value();
    } else {
        return std::make_shared<Undefined>();
    }
}

std::shared_ptr<Node> Node::getElementEx(std::size_t index) const {
    std::optional<std::shared_ptr<Node>> v = this->getElementO(index);
    if (v.has_value()) {
        return v.value();
    } else {
        throw tu::Exception("Element \"" + std::to_string(index) + "\" does not exist", EXCTX);
    }
}

std::optional<std::shared_ptr<Node>> Node::getElementO(std::size_t index) const {
    return std::optional<std::shared_ptr<Node>>();
}

bool Node::hasElement(std::size_t index) const {
    return getElementO(index).has_value();
}

std::size_t Node::size() const {
    return 0;
}

Node* Node::setElement(std::size_t index, std::shared_ptr<Node> node) {
    return this;
}



void Node::toStream(std::ostream& stream) {
    nlohmann::json j = this->toNlohmann();
    stream << j;
}

std::ostream& operator<<(std::ostream& stream, std::shared_ptr<Node> instance) {
    instance->toStream(stream);
    return stream;
}

std::string Node::toJSONString() {
    std::stringstream ss;
    ss << this->toNlohmann();
    return ss.str();
}

void Node::toFile(std::string& filename) {
    std::ofstream file;
    file.open(filename);
    file << this->toNlohmann();
}





std::shared_ptr<Node> Node::fromNlohmann(const nlohmann::json& j) {
    if (j.is_array()) {
        return Array::fromNlohmann(j);
    } else if (j.is_object()) {
        return Object::fromNlohmann(j);
    } else {
        return Value::fromNlohmann(j);
    }
}

std::shared_ptr<Node> Node::fromJSON(const std::string& json) {
    auto v = fromJSONO(json);
    if (v.has_value()) {
        return v.value();
    } else {
        return std::make_shared<Undefined>();
    }
}

std::shared_ptr<Node> Node::fromFile(const std::string& filename) {
    auto v = fromFileO(filename);
    if (v.has_value()) {
        return v.value();
    } else {
        return std::make_shared<Undefined>();
    }
}



std::shared_ptr<Node> Node::fromJSONEx(const std::string& json) {
    nlohmann::json j;
    try {
        j = nlohmann::json::parse(json);
    } catch (nlohmann::json::exception& e) {
        throw Exception(e.what(), e.id, EXCTX);
    }
    return fromNlohmann(j);
}

std::shared_ptr<Node> Node::fromFileEx(const std::string& filename) {
    std::ifstream file;
    file.open(filename);

    nlohmann::json j;
    try {
        j = nlohmann::json::parse(file);
    } catch (nlohmann::json::exception& e) {
        throw Exception(e.what(), e.id, EXCTX);
    }
    return fromNlohmann(j);
}



std::optional<std::shared_ptr<Node>> Node::fromJSONO(const std::string& json) {
    nlohmann::json j;
    try {
        j = nlohmann::json::parse(json);
    } catch (nlohmann::json::exception& e) {
        return std::optional<std::shared_ptr<Node>>();
    }
    return std::optional(fromNlohmann(j));
}

std::optional<std::shared_ptr<Node>> Node::fromFileO(const std::string& filename) {
    std::ifstream file;
    file.open(filename);

    nlohmann::json j;
    try {
        j = nlohmann::json::parse(file);
    } catch (nlohmann::json::exception& e) {
        return std::optional<std::shared_ptr<Node>>();
    }
    return std::optional(fromNlohmann(j));
}





Node::Node(Type type) {
    this->type = type;
}



}

}
