#include "Log.hpp"



namespace tu {


bool Log::open(std::string filename) {
    file.open(filename, std::ios::app);
    fileOpen = file.is_open();
    return !fileOpen;
}

void Log::close() {
    file.close();
    fileOpen = false;
}

std::mutex Log::printLock;

std::ofstream& Log::getFile() {
    if (!fileOpen) {
        if (Log::open("all.log")) {
            file << "====================\n";
        } else {
            std::cerr << "Log: Error while opening log file\n";
        }
    }
    return file;
}

bool Log::lock = false;
bool Log::fileOpen = false;
std::ofstream Log::file = std::ofstream();


};