#include "Timestamp.hpp"


#include <sstream>
#include <iomanip>



namespace tu {


Timestamp Timestamp::now() {
    return Timestamp(std::chrono::system_clock::now());
}


std::time_t Timestamp::getUNIXTimestamp() const {
    return std::chrono::system_clock::to_time_t(this->timePoint);
}

std::chrono::system_clock::time_point Timestamp::getTimePoint() const {
    return this->timePoint;
}

Date Timestamp::getDate() const {
    return Date(this->timePoint);
}



Timestamp& Timestamp::setUNIXTimestamp(const std::time_t& timestamp) {
    auto epoch = std::chrono::time_point<std::chrono::system_clock>();
    auto sinceEpoch = std::chrono::milliseconds(timestamp);
    this->timePoint = epoch + sinceEpoch;
    return *this;
}

Timestamp& Timestamp::setTimePoint(const std::chrono::system_clock::time_point& timePoint) {
    this->timePoint = timePoint;
    return *this;
}

Timestamp& Timestamp::setDate(const Date& date) {
    return setTimePoint(date.getTimePoint());
}


// Convert to a UNIX timestamp
Timestamp::operator std::time_t() const {
    return this->getUNIXTimestamp();
}

// Convert to a std::chrono::time_point
Timestamp::operator std::chrono::system_clock::time_point() const {
    return this->getTimePoint();
}

// Convert to a Date
Timestamp::operator Date() const {
    return this->getDate();
}



Timestamp::Timestamp() {
    setUNIXTimestamp(0);
}

Timestamp::Timestamp(const std::time_t& timestamp) {
    setUNIXTimestamp(timestamp);
}

Timestamp::Timestamp(const std::chrono::system_clock::time_point& timePoint) {
    setTimePoint(timePoint);
}

Timestamp::Timestamp(const Date& date) {
    setDate(date);
}



std::chrono::milliseconds Timestamp::diff(const Timestamp& timestamp) const {
    return this->diff<>(timestamp);
}

std::chrono::duration<int64_t, std::nano> Timestamp::operator-(const Timestamp& timestamp) const {
    return this->timePoint - timestamp.timePoint;
}

bool Timestamp::operator==(const Timestamp& timestamp) const {
    return this->timePoint == timestamp.timePoint;
}

bool Timestamp::operator!=(const Timestamp& timestamp) const {
    return this->timePoint != timestamp.timePoint;
}

bool Timestamp::operator<=(const Timestamp& timestamp) const {
    return this->timePoint <= timestamp.timePoint;
}

bool Timestamp::operator<(const Timestamp& timestamp) const {
    return this->timePoint < timestamp.timePoint;
}

bool Timestamp::operator>=(const Timestamp& timestamp) const {
    return this->timePoint >= timestamp.timePoint;
}

bool Timestamp::operator>(const Timestamp& timestamp) const {
    return this->timePoint > timestamp.timePoint;
}



std::string Timestamp::toString() const {
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(this->timePoint.time_since_epoch()) % 1000;

    auto timet = std::chrono::system_clock::to_time_t(this->timePoint);
    std::tm brokenTime = *std::localtime(&timet);

    std::stringstream ss;
    ss << std::put_time(&brokenTime, "%H:%M:%S"); // HH:MM:SS
    ss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    return ss.str();
}


};
