#pragma once


#include "PausableClock.hpp"



namespace tu {


// [TODO]
template<class Ratio = std::milli, typename UnitType = int64_t, bool PString = true>
class TickClock : protected PausableClock<Ratio, UnitType, PString> {


public:
    double getTPS();
    unsigned int getMaxMultiTicks();

    void setTPS(double tps);
    void setMaxMultiTicks(unsigned int maxTicks);

    void setOverloadCallback(void (*callback)) {
        this->overloadCB = callback;
    }



    UnitType get() {
        return 0;
    }

    void set(UnitType value) {

    }

    void reset() {

    }

    void advance(UnitType delta) {

    }

    void setPaused(bool paused, UnitType exactTime) {

    }

    bool isPaused() {
        return false;
    }


    TickClock(double tps, unsigned int maxTicks = 100) {
        this->tps = tps;
        this->maxTicks = maxTicks;
    }

protected:
    // Ticks per second
    double tps;

    // Maximum number of ticks in one clock cycle
    unsigned int maxTicks;

    // Callback to call when maxTicks is reached or surpassed
    void (*overloadCB)();


};


};
