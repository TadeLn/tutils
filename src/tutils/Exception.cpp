#include "Exception.hpp"


#include "Log.hpp"



namespace tu {



std::string Exception::getMessage() {
    return message;
}

int Exception::getCode() {
    return code;
}


void Exception::print(Level logLevel, std::string preMessage) {
    if (!preMessage.empty()) {
        preMessage.append(": ");
    }
    switch (logLevel) {
        case LEVEL_ERROR:
            Log::error(preMessage, "An exception occurred: ", message, " @ ", file, ":", line, " (exception code: ", code, ")");
            break;

        case LEVEL_WARNING:
            Log::warn(preMessage, "An exception occurred: ", message, " @ ", file, ":", line, " (exception code: ", code, ")");
            break;

        case LEVEL_INFO:
            Log::info(preMessage, "An exception occurred: ", message, " @ ", file, ":", line, " (exception code: ", code, ")");
            break;

        case LEVEL_LOG:
        default:
            Log::log(preMessage, "An exception occurred: ", message, " @ ", file, ":", line, " (exception code: ", code, ")");
            break;
    }
}

void Exception::print(std::string file, int line, Level logLevel, std::string preMessage) {
    this->print(logLevel, preMessage);
    Log::info("(caught @ ", file, ":", line, " )");
}


Exception::Exception(const std::exception& exception)
    : Exception (exception.what(), STD_EXCEPTION, EXCTX)
{}

Exception::Exception(std::string file, int line)
    : Exception(UNKNOWN, file, line)
{}

Exception::Exception(std::string message, std::string file, int line)
    : Exception(message, UNKNOWN, file, line)
{}

Exception::Exception(int code, std::string file, int line)
    : Exception(std::string(), code, file, line)
{}

Exception::Exception(std::string message, int code, std::string file, int line) {
    this->file = file;
    this->line = line;
    this->message = message;
    this->code = code;
}


void Exception::_assert(bool check, std::string file, int line) {
    if (!check) {
        throw Exception("Assertion failed", ASSERTION_FAILED, file, line);
    }
}

void Exception::_assert(bool check, Exception ex) {
    if (!check) {
        throw ex;
    }
}


};