#pragma once


#include <chrono>
#include <sstream>
#include <iomanip>

#include "Stringable.hpp"



namespace tu {


template<class Ratio = std::milli, typename UnitType = int64_t>
class Clock : public Stringable {


public:
    virtual UnitType get() = 0;
    virtual void set(UnitType value) = 0;
    virtual void reset() = 0;
    virtual void advance(UnitType delta) {};
    
    virtual std::string toString() const {
        std::stringstream ss;
        ss << "<" << this->formatTime(this->get()) << ">";
        return ss.str();
    }


    static std::string formatTime(UnitType units) {
        double ratioD = (double)Ratio().num / (double)Ratio().den;
        double seconds = ratioD * units;
        int secondsI = abs(seconds);
        double secondsAbs = (seconds < 0) ? -seconds : seconds;
        double secondsFrac = secondsAbs - secondsI;

        std::stringstream ss;
        ss << std::setfill('0');
        // ss << "{RAW:" << seconds << "}";

        if (seconds < 0) {
            ss << "-";
        }

        int hours = secondsI / 3600;
        if (hours != 0) {
            // ss << "{hours:" << hours << "}";
            ss << abs(hours) << ":" << std::setw(2);
        }

        int minutes = (secondsI / 60) % 60;
        if (hours != 0 || minutes != 0) {
            // ss << "{minutes:" << minutes << "}";
            ss << abs(minutes) << std::setw(0) << ":" << std::setw(2);
        }

        int secondsOnly = secondsI % 60;
        // ss << "{secondsOnly:" << secondsOnly << "}";
        ss << abs(secondsOnly);
        
        // ss << "{ratioD:" << ratioD << "}";
        if (ratioD < 0.0009) {
            // ss << "{secondsFrac:" << (secondsFrac * 1000000) << "}";
            ss << std::setw(0) << "." << std::setw(6) << (int)(secondsFrac * 1000000);
        } else if (ratioD < 1) {
            ss << std::setw(0) << "." << std::setw(3) << (int)(secondsFrac * 1000);
        }
        return ss.str();
    }


};


};
