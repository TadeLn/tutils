#pragma once


#include <chrono>

#include "Stringable.hpp"

namespace tu {
    class Date;
};

#include "Timestamp.hpp"



namespace tu {
    

class Date : public Stringable {


public:

    // Current date timestamp
    static Date now();


    // Get UNIX timestamp
    std::time_t getUNIXTimestamp() const;

    // Get std::chrono time point
    std::chrono::system_clock::time_point getTimePoint() const;

    // Get Timestamp
    Timestamp getTimestamp() const;
    

    // Set UNIX timestamp
    Date& setUNIXTimestamp(const std::time_t& timestamp);

    // Set std::chrono time point
    Date& setTimePoint(const std::chrono::system_clock::time_point& timePoint);

    // Set from a Timestamp
    Date& setTimestamp(const Timestamp& timestamp);


    // Convert to a UNIX timestamp
    operator std::time_t() const;

    // Convert to a std::chrono::time_point
    operator std::chrono::system_clock::time_point() const;

    // Convert to a Date
    operator Timestamp() const;


    // Construct a new date
    Date();

    // Construct a date from a timetimestamp
    Date(const std::time_t& timestamp);

    // Construct a date from a time point
    Date(const std::chrono::system_clock::time_point& timePoint);

    // Construct a date from a Timestamp
    Date(const Timestamp& timestamp);


    // Convert to string
    std::string toString() const;

    // Convert to string in `yyyy_mm_dd_hh_mm_ss` format
    std::string toSnakeCaseString() const;


private:

    // Seconds since epoch
    std::time_t timestamp;

};


};
