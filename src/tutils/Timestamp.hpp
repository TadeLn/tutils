#pragma once


#include <chrono>

#include "Stringable.hpp"

namespace tu {
    class Timestamp;
};

#include "Date.hpp"



namespace tu {
    

class Timestamp : public Stringable {


public:

    // Current timestamp
    static Timestamp now();


    // Get UNIX timestamp
    std::time_t getUNIXTimestamp() const;

    // Get std::chrono time point
    std::chrono::system_clock::time_point getTimePoint() const;

    // Get Date
    Date getDate() const;
    

    // Set UNIX timestamp
    Timestamp& setUNIXTimestamp(const std::time_t& timestamp);

    // Set from a std::chrono::time_point
    Timestamp& setTimePoint(const std::chrono::system_clock::time_point& timePoint);

    // Set from a Date
    Timestamp& setDate(const Date& date);


    // Convert to a UNIX timestamp
    operator std::time_t() const;

    // Convert to a std::chrono::time_point
    operator std::chrono::system_clock::time_point() const;

    // Convert to a Date
    operator Date() const;


    // Construct a new timestamp
    Timestamp();

    // Construct a timestamp from a UNIX timestamp
    Timestamp(const std::time_t& timestamp);

    // Construct a timestamp from a time point
    Timestamp(const std::chrono::system_clock::time_point& timePoint);

    // Construct a timestamp from a Date
    Timestamp(const Date& date);


    template<typename Rep, typename Period>
    Timestamp operator+(const std::chrono::duration<Rep, Period>& duration) const {
        return Timestamp(this->timePoint + duration);
    }

    template<typename Rep, typename Period>
    void operator+=(const std::chrono::duration<Rep, Period>& duration) {
        this->timePoint += duration;
    }

    template<typename Rep, typename Period>
    Timestamp operator-(const std::chrono::duration<Rep, Period>& duration) const {
        return Timestamp(this->timePoint - duration);
    }

    template<typename Rep, typename Period>
    void operator-=(const std::chrono::duration<Rep, Period>& duration) {
        this->timePoint -= duration;
    }


    std::chrono::milliseconds diff(const Timestamp& timestamp) const;

    template<typename Duration = std::chrono::milliseconds>
    Duration diff(const Timestamp& timestamp) const {
        return std::chrono::duration_cast<Duration>(timestamp - (*this));
    }

    std::chrono::duration<int64_t, std::nano> operator-(const Timestamp& timestamp) const;


    bool operator==(const Timestamp& timestamp) const;

    bool operator!=(const Timestamp& timestamp) const;

    bool operator<=(const Timestamp& timestamp) const;

    bool operator<(const Timestamp& timestamp) const;

    bool operator>=(const Timestamp& timestamp) const;

    bool operator>(const Timestamp& timestamp) const;


    // Convert to string
    std::string toString() const;


private:

    // Precise time point
    std::chrono::system_clock::time_point timePoint;

};


};
