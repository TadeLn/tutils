#pragma once


#include <string>



#ifndef EXCTX
#define EXCTX std::string(__FILE__), __LINE__
#endif


namespace tu {


class Exception {

public:
    enum Codes {
        UNKNOWN = -1,
        OK,
        INDEX_OUT_OF_BOUNDS,
        METHOD_NOT_IMPLEMENTED,
        ASSERTION_FAILED,
        VALUE_NOT_FOUND,
        VALUE_ALREADY_EXISTS,
        TOO_LITTLE_VALUES,
        TOO_MUCH_VALUES,
        STD_EXCEPTION,
        BAD_VALUE
    };

    enum Level {
        LEVEL_LOG = 0,
        LEVEL_INFO,
        LEVEL_WARNING,
        LEVEL_ERROR
    };


    // Get filename
    std::string getFile();

    // Get line number
    int getLine();

    // Get exception message
    std::string getMessage();

    // Get exception code
    int getCode();


    // Print message to console
    void print(Level logLevel = Level::LEVEL_ERROR, std::string preMessage = "");

    // Print message to console
    void print(std::string file, int line, Level logLevel = Level::LEVEL_ERROR, std::string preMessage = "");


    // Convert std::exception to Exception
    Exception(const std::exception& exception);

    // Construct the exception
    Exception(std::string file, int line);

    // Construct the exception with a message
    Exception(std::string message, std::string file, int line);

    // Construct the exception with an exception code
    Exception(int code, std::string file, int line);

    // Construct the exception with a message and an exception code
    Exception(std::string message, int code, std::string file, int line);


    // Throw an exception if the statement is false
    static void _assert(bool check, std::string file, int line);

    // Throw an exception if the statement is false
    static void _assert(bool check, Exception ex);


private:

    // Location of the exception (file)
    std::string file;

    // Location of the exception (line)
    int line;

    // Message of the exception
    std::string message;

    // Code of the exception
    int code;

};


};
