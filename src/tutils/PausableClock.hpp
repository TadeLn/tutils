#pragma once


#include "Clock.hpp"



namespace tu {


template<class Ratio = std::milli, typename UnitType = int64_t, bool PString = true>
class PausableClock : public Clock<Ratio, UnitType> {


public:
    void pause() {
        this->setPaused(true);
    }

    void pause(UnitType exactTime) {
        this->setPaused(true, exactTime);
    }

    void unpause() {
        this->setPaused(false);
    }

    void unpause(UnitType exactTime) {
        this->setPaused(false, exactTime);
    }

    void setPaused(bool paused) {
        this->setPaused(paused, this->get());
    }

    virtual void setPaused(bool paused, UnitType exactTime) = 0;

    virtual bool isPaused() = 0;

    virtual std::string toString() {
        std::stringstream ss;
        ss << "<";
        if (PString) {
            if (this->isPaused()) {
                ss << "(P) ";
            }
        }
        ss << this->formatTime(this->get()) << ">";
        return ss.str();
    }

};


};
