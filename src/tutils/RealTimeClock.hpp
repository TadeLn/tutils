#pragma once


#include <chrono>

#include "PausableClock.hpp"



namespace tu {


template<class Ratio = std::micro, typename UnitType = int64_t, bool PString = true>
class RealTimeClock : public PausableClock<Ratio, UnitType, PString> {


public:
    UnitType get() {
        return this->get(std::chrono::steady_clock::now());
    }

    UnitType get(std::chrono::steady_clock::time_point tp) {
        if (this->isPaused()) {
            return std::chrono::duration_cast<std::chrono::duration<UnitType, Ratio>>((this->pauseStartTime - this->startTime) - this->pauseDuration).count();
        } else {
            return std::chrono::duration_cast<std::chrono::duration<UnitType, Ratio>>((tp - this->startTime) - this->pauseDuration).count();
        }
    }

    void set(UnitType value) {
        auto now = std::chrono::steady_clock::now();
        this->pauseStartTime = now;
        this->pauseDuration = std::chrono::milliseconds(0);
        this->startTime = now - std::chrono::duration<UnitType, Ratio>(value);
    }

    void reset() {
        auto now = std::chrono::steady_clock::now();
        this->pauseStartTime = now;
        this->pauseDuration = std::chrono::milliseconds(0);
        this->startTime = now;
    }

    void setPaused(bool paused, UnitType exactTime) {
        UnitType currentTime = this->get();
        auto now = std::chrono::steady_clock::now() - std::chrono::duration<UnitType, Ratio>(currentTime - exactTime);
        this->paused = paused;
        if (paused) {
            this->pauseStartTime = now;
        } else {
            this->pauseDuration += now - this->pauseStartTime;
        }
    }

    bool isPaused() {
        return paused;
    }

    RealTimeClock() {
        paused = false;
        this->reset();
    }

protected:
    std::chrono::steady_clock::time_point startTime;

    bool paused;
    std::chrono::steady_clock::time_point pauseStartTime;
    std::chrono::steady_clock::duration pauseDuration;


};


};
