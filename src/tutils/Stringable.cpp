#include "Stringable.hpp"



namespace tu {


std::string Stringable::str() const {
    return this->toString();
}

Stringable::operator std::string() {
    return this->toString();
}

std::ostream& operator<<(std::ostream& stream, const Stringable& instance) {
    stream << instance.toString();
    return stream;
}


};
