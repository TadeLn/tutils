#pragma once


#include <string>



namespace tu {
    

class Stringable {

public:
    // Convert the object to string
    virtual std::string toString() const = 0;
    
    // Alias to Stringable::toString();
    std::string str() const;

    // Cast the object to string
    operator std::string();

    // Add the object to a stream
    friend std::ostream& operator<<(std::ostream& stream, const Stringable& instance);

};


};
