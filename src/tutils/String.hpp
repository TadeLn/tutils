#pragma once


#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <memory>

#include "Stringable.hpp"
#include "Byte.hpp"



namespace tu {


class String {

public:

    template<typename First>
    static std::string str(const First& first) {
        std::stringstream ss;
        ss << first;
        return ss.str();
    }

    static std::string str(const Stringable& first) {
        return first.toString();
    }

    static std::string str(const bool& first) {
        return first ? "true" : "false";
    }

    static std::string str(const Byte& first) {
        return std::to_string((unsigned int)first);
    }

    static std::string str(const char& first) {
        return std::to_string(first);
    }

    static std::string str(const short& first) {
        return std::to_string(first);
    }

    static std::string str(const int& first) {
        return std::to_string(first);
    }

    static std::string str(const long& first) {
        return std::to_string(first);
    }

    static std::string str(const float& first) {
        return std::to_string(first);
    }

    static std::string str(const double& first) {
        return std::to_string(first);
    }

    static std::string str(const std::string& first) {
        return first;
    }
    
    template<typename T>
    static std::string str(const std::vector<T>& vector) {
        return String::fromVector(vector);
    }
    
    template<typename K, typename V>
    static std::string str(const std::map<K, V>& map) {
        return String::fromMap(map);
    }
    
    template<typename T, typename U>
    static std::string str(const std::pair<T, U>& vector) {
        return String::fromPair(vector);
    }

    // Stringify and concat multiple values at once
    template<typename ...Args>
    static std::string str(const Args& ...args) {
        std::stringstream ss;
        toSS(ss, args...);
        return ss.str();
    }


    template<typename First>
    static void toSS(std::stringstream& ss, const First& first) {
        ss << str(first);
    }
    
    template<typename First, typename ...Last>
    static void toSS(std::stringstream& ss, const First& first, const Last& ...last) {
        toSS(ss, first);
        toSS(ss, last...);
    }

    

    template<typename T, typename Callback>
    static std::string fromVector(const std::vector<T>& vector, Callback callback) {
        std::string result = String::str("(", vector.size(), ")[");
        for (unsigned int i = 0; i < vector.size(); i++) {
            if (i > 0) {
                result.append(", ");
            }
            result.append(callback(vector[i], i));
        }
        result.append("]");
        return result;
    }

    template<typename T>
    static std::string fromVector(const std::vector<T>& vector) {
        std::string result = String::str("(", vector.size(), ")[");
        for (unsigned int i = 0; i < vector.size(); i++) {
            if (i > 0) {
                result.append(", ");
            }
            result.append(String::str(vector[i]));
        }
        result.append("]");
        return result;
    }


    template<typename K, typename V, typename Callback>
    static std::string fromMap(const std::map<K, V>& map, Callback callback) {
        std::string result = "{";
        bool first = true;
        for (auto pair : map) {
            if (!first) {
                result.append(", ");
            } else {
                first = false;
            }
            result.append(callback(pair.first, pair.second));
        }
        result.append("}");
        return result;
    }

    template<typename K, typename V>
    static std::string fromMap(const std::map<K, V>& map) {
        std::string result = "{";
        bool first = true;
        for (auto pair : map) {
            if (!first) {
                result.append(", ");
            } else {
                first = false;
            }
            result.append(String::str(pair.first, "=", pair.second));
        }
        result.append("}");
        return result;
    }

    template<typename T, typename U>
    static std::string fromPair(const std::pair<T, U>& pair) {
        return String::str("(", pair.first, ", ", pair.second, ")");
    }

};


};
